﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _439MMO.Models
{
    public class CreateUser
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}