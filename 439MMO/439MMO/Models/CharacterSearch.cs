﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _439MMO.Models
{
    public class CharacterSearch
    {
        [Display(Name = "Search Character")]
        public string CharName { get; set; }
    }
}