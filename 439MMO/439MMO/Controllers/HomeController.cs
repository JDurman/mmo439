﻿using _439MMO.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _439MMO.Controllers
{
    public class HomeController : Controller
    {
        exoloed_MMORPGEntities1 context = new exoloed_MMORPGEntities1();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UserInfo(CharacterSearch character)
        {
            List<_439MMO.PlayerCharacter> returnCharacter = null;
            if (ModelState.IsValid)
            {
                returnCharacter = context.PlayerCharacters.Where(x => x.Name == character.CharName).ToList();
            }
            else
            {
                return RedirectToAction("Index");
            }

            return View(returnCharacter);
        }

        public ActionResult CreateCharacter()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateCharacter(_439MMO.PlayerCharacter character)
        {
            if(ModelState.IsValid)
            {
                context.PlayerCharacters.Add(character);
            }
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult ListProfessions()
        {
            var professions = context.Professions.Where(x => x.ID != null);
            return View(professions);
        }

        public ActionResult ListClasses()
        {
            var classes = context.Classes.Where(x => x.ID != null).ToList();
            return View(classes);
        }

        public ActionResult ListSpecializations()
        {
            var specializations = context.Specializations.Where(x => x.ID != null).ToList();
            return View(specializations);
        }

        public ActionResult ListRaces()
        {
            var races = context.Races.Where(x => x.ID != null).ToList();
            return View(races);
        }

        public ActionResult ListRealms()
        {
            var realms = context.Realms.Where(x => x.ID != null).ToList();
            return View(realms);
        }

        public ActionResult CreateAccount()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateAccount(_439MMO.Account user)
        {
            if (ModelState.IsValid)
            {
                context.Accounts.Add(user);
            }
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}